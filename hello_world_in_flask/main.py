from flask import Flask

app = Flask(__name__)


@app.route('/')
def page_root():
    s_page = "<html>"
    s_page += "<title>Hello World</title>"
    s_page += "<body>"
    s_page += "<h1>Hello World!</h1>"
    s_page += "</body>"
    s_page += "</html>"

    return s_page


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8181, debug=True)
