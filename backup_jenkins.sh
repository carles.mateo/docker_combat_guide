#!/usr/bin/env bash

s_DATE=$(date +"%Y-%m-%d")

s_DOCKER_IMG_JENKINS_EXPORT=${s_DATE}-jenkins-base.tar

# Save Docker Images
sudo docker save jenkins:base --output /home/carles/Desktop/Docker_Images/${s_DOCKER_IMG_JENKINS_EXPORT}
gzip /home/carles/Desktop/Docker_Images/${s_DOCKER_IMG_JENKINS_EXPORT}
